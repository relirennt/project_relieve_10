import express from 'express';

// Map
import { routerMap as dataRouterMap } from './backend/map/map.routes.js';
import { getDatasMap } from './backend/map/map.controller.js';

// Diagramm 1
import { routerDiagram1 as dataRouterDiagram1 } from './backend/diagram1/diagram1.routes.js';
import { getDatasDiagram1 } from './backend/diagram1/diagram1.controller.js';

// Diagramm 2
import { routerDiagram2 as dataRouterDiagram2 } from './backend/diagram2/diagram2.routes.js';
import { getDatasDiagram2 } from './backend/diagram2/diagram2.controller.js';

// Diagramm 3
import { routerDiagram3 as dataRouterDiagram3 } from './backend/diagram3/diagram3.routes.js';
import { getDatasDiagram3 } from './backend/diagram3/diagram3.controller.js';

const app = express();

app.use(express.static('frontend'));

app.use(express.json());

// Map
app.use('/api/map', dataRouterMap);
getDatasMap();

// Diagramm 1
app.use('/api/diagram1', dataRouterDiagram1);
getDatasDiagram1();

// Diagramm 2
app.use('/api/diagram2', dataRouterDiagram2);
getDatasDiagram2();

// Diagramm 3
app.use('/api/diagram3', dataRouterDiagram3);
getDatasDiagram3();

app.listen(3001, () => {
  console.log('Server listens to http://localhost:3001');
});