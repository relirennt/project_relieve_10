const datasMap = [];

  function getAllMap() {
    return Promise.resolve(datasMap);
  }

  function createDataMap(data) {
    datasMap.push(data);
  }

  export { getAllMap, createDataMap };