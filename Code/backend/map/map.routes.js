import { Router } from 'express';
import { getVotesMap } from './map.controller.js';

const routerMap = Router();

routerMap.get('/', getVotesMap);

export { routerMap };