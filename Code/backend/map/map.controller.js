import axios from "axios";
import { createDataMap, getAllMap } from "./map.model.js";

function getDatasMap() {
  axios
    .get("https://data.tg.ch/api/v2/catalog/datasets/sk-stat-52/exports/json")
    .then((response) => {
      const datasMap = response.data;
      const filteredDatasMap = datasMap.filter(
        (rawDataMap) => rawDataMap.datum_abstimmung === "2004-05-16"
      );
      for (let i = 0; i < filteredDatasMap.length; i++) {
        const rawDataMap = filteredDatasMap[i];
        const dataMap = {
          bfs_nr_gemeinde: rawDataMap.bfs_nr_gemeinde,
          gemeinde_name: rawDataMap.gemeinde_name,
          stimmberechtigte: rawDataMap.stimmberechtigte,
          gueltige_stimmen: rawDataMap.gueltige_stimmen,
          ungueltige_stimmen: rawDataMap.ungueltige_stimmen,
          stimmbeteiligung: rawDataMap.stimmbeteiligung,
          ja_stimmen: rawDataMap.ja_stimmen,
          nein_stimmen: rawDataMap.nein_stimmen,
          datum_abstimmung: rawDataMap.datum_abstimmung,
        };
        createDataMap(dataMap);
      }
    });
}

const getVotesMap = async (req, res) => {
  const dataMap = await getAllMap();
  res.json(dataMap);
};

export { getDatasMap, getVotesMap };
