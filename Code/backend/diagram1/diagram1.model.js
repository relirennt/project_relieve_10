const datasDiagram1 = [];

  function getAllDiagram1() {
    return Promise.resolve(datasDiagram1);
  }

  function createDataDiagram1(data) {
    datasDiagram1.push(data);
  }

  export { getAllDiagram1, createDataDiagram1 };