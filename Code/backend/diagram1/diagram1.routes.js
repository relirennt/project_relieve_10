import { Router } from "express";
import { getVotesDiagram1 } from "./diagram1.controller.js";

const routerDiagram1 = Router();

routerDiagram1.get("/", getVotesDiagram1);

export { routerDiagram1 };
