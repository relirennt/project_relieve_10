import axios from "axios";
import { createDataDiagram1, getAllDiagram1 } from "./diagram1.model.js";

async function getDatasDiagram1() {
  axios
    .get("https://data.tg.ch/api/v2/catalog/datasets/sk-stat-52/exports/json")
    .then((response) => {
      const datasDiagram1 = response.data;
      const filteredDatasDiagram1 = datasDiagram1.filter(
        (rawDataDiagram1) => rawDataDiagram1.datum_abstimmung === "2004-05-16"
      );
      for (let i = 0; i < filteredDatasDiagram1.length; i++) {
        const rawDataDiagram1 = filteredDatasDiagram1[i];
        const dataDiagram1 = {
          gemeinde_name: rawDataDiagram1.gemeinde_name,
          bfs_nr_gemeinde: rawDataDiagram1.bfs_nr_gemeinde,
          eingelegte_stimmzettel: rawDataDiagram1.eingelegte_stimmzettel,
          gueltige_stimmen: rawDataDiagram1.gueltige_stimmen,
          ungueltige_stimmen: rawDataDiagram1.ungueltige_stimmen,
          leere_stimmen: rawDataDiagram1.leere_stimmen,
          datum_abstimmung: rawDataDiagram1.datum_abstimmung,
        };
        createDataDiagram1(dataDiagram1);
      }
    });
}

const getVotesDiagram1 = async (req, res) => {
  const dataDiagram1 = await getAllDiagram1();
  res.json(dataDiagram1);
};

export { getDatasDiagram1, getVotesDiagram1 };
