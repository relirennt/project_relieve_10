const datasDiagram2 = [];

  function getAllDiagram2() {
    return Promise.resolve(datasDiagram2);
  }

  function createDataDiagram2(data) {
    datasDiagram2.push(data);
  }

  export { getAllDiagram2, createDataDiagram2 };