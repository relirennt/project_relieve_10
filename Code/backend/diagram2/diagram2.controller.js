import axios from "axios";
import { createDataDiagram2, getAllDiagram2 } from "./diagram2.model.js";

function getDatasDiagram2() {
  axios
    .get("https://data.tg.ch/api/v2/catalog/datasets/sk-stat-52/exports/json")
    .then((response) => {
      const datasDiagram2 = response.data;
      const filteredDatasDiagram2 = datasDiagram2.filter(
        (rawDataDiagram2) => rawDataDiagram2.datum_abstimmung === "2004-05-16"
      );
      for (let i = 0; i < filteredDatasDiagram2.length; i++) {
        const rawDataDiagram2 = filteredDatasDiagram2[i];
        const dataDiagram2 = {
          bfs_nr_gemeinde: rawDataDiagram2.bfs_nr_gemeinde,
          gemeinde_name: rawDataDiagram2.gemeinde_name,
          ja_stimmen: rawDataDiagram2.ja_stimmen,
          nein_stimmen: rawDataDiagram2.nein_stimmen,
          ungueltige_stimmen: rawDataDiagram2.ungueltige_stimmen,
          leere_stimmen: rawDataDiagram2.leere_stimmen,
          datum_abstimmung: rawDataDiagram2.datum_abstimmung,
        };
        createDataDiagram2(dataDiagram2);
      }
    });
}

const getVotesDiagram2 = async (req, res) => {
  const dataDiagram2 = await getAllDiagram2();
  res.json(dataDiagram2);
};

export { getDatasDiagram2, getVotesDiagram2 };
