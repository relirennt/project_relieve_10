import { Router } from 'express';
import { getVotesDiagram2 } from './diagram2.controller.js';

const routerDiagram2 = Router();

routerDiagram2.get('/', getVotesDiagram2);

export { routerDiagram2 };