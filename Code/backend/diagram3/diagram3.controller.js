import axios from "axios";
import { createDataDiagram3, getAllDiagram3 } from "./diagram3.model.js";

function getDatasDiagram3() {
  axios
    .get("https://data.tg.ch/api/v2/catalog/datasets/sk-stat-52/exports/json")
    .then((response) => {
      const datasDiagram3 = response.data;
      const filteredDatasDiagram3 = datasDiagram3.filter(
        (rawDataDiagram3) => rawDataDiagram3.datum_abstimmung === "2004-05-16"
      );
      for (let i = 0; i < filteredDatasDiagram3.length; i++) {
        const rawDataDiagram3 = filteredDatasDiagram3[i];
        const dataDiagram3 = {
          bezirk_nr: rawDataDiagram3.bezirk_nr,
          bezirk_name: rawDataDiagram3.bezirk_name,
          stimmbeteiligung: rawDataDiagram3.stimmbeteiligung,
        };

        createDataDiagram3(dataDiagram3);
      }
    });
}

const getVotesDiagram3 = async (req, res) => {
  const dataDiagram3 = await getAllDiagram3();
  res.json(dataDiagram3);
};

export { getDatasDiagram3, getVotesDiagram3 };
