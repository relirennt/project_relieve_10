const datasDiagram3 = [];

  function getAllDiagram3() {
    return Promise.resolve(datasDiagram3);
  }

  function createDataDiagram3(data) {
    datasDiagram3.push(data);
  }

  export { getAllDiagram3, createDataDiagram3 };