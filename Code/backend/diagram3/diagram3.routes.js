import { Router } from 'express';
import { getVotesDiagram3 } from './diagram3.controller.js';

const routerDiagram3 = Router();

routerDiagram3.get('/', getVotesDiagram3);

export { routerDiagram3 };