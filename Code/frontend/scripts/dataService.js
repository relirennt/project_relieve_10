function getAllMap() {
  const request = axios.get("/api/map");
  return request.then((response) => response.data);
}

function getAllDiagram1() {
  const request = axios.get("/api/diagram1");
  return request.then((response) => response.data);
}

function getAllDiagram2() {
  const request = axios.get("/api/diagram2");
  return request.then((response) => response.data);
}

function getAllDiagram3() {
  const request = axios.get("/api/diagram3");
  return request.then((response) => response.data);
}

export { getAllMap, getAllDiagram1, getAllDiagram2, getAllDiagram3 };
