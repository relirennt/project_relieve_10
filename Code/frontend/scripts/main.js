import { createLineChart } from "./lineChart.js";
import { createBarChart } from "./barChart.js";
import { createPieChart } from "./pieChart.js";
import {
  getAllMap,
  getAllDiagram1,
  getAllDiagram2,
  getAllDiagram3,
} from "./dataService.js";

SVGInject.setOptions({ makeIdsUnique: false });
const elMapTg = document.getElementById("tg-map");
await SVGInject(elMapTg);

let bfs_num;

// Zu Beginn alle Map Daten vom Backend laden
getAllMap().then((dataMap) => {
  console.log("Map-Diagramm");
  console.log(dataMap); //
  for (let i = 0; i < dataMap.length; i++) {
    let x = document.getElementById(dataMap[i].bfs_nr_gemeinde);
    let ja_stimmen = dataMap[i].ja_stimmen;
    let nein_stimmen = dataMap[i].nein_stimmen;
    if (ja_stimmen > nein_stimmen) {
      x.classList.add("area_green");
    }
    x.addEventListener("mouseover", (event) => {
      const elTooltip = document.getElementById("tooltip-map");
      elTooltip.classList.remove("do-not-display");
      elTooltip.classList.add("displayed");
      elTooltip.style.top = `${event.pageY}px`;
      elTooltip.style.left = `${event.pageX}px`;
      let bfs_num = dataMap[i].bfs_nr_gemeinde;
      const bfs_sel = document.getElementById(bfs_num);
      elTooltip.innerHTML = `<div style="display: flex; flex-direction: column; font-family: 'Roboto', sans-serif; color: #152238;">
          <h1 style="margin-top: 0; font-size: 24px; font-weight: bold;">${
            dataMap[i].gemeinde_name
          }</h1>
          <div style="margin-top: auto;">
              <div style="display: flex; justify-content: space-between; margin-bottom: 8px;">
                  <span style="font-weight: bold;">Stimmberechtigte:</span>
                  <span>&nbsp;&nbsp;${dataMap[i].stimmberechtigte}</span>
              </div>
              <div style="display: flex; justify-content: space-between; margin-bottom: 8px;">
                  <span style="font-weight: bold;">Gültige Stimmen:</span>
                  <span>&nbsp;&nbsp;${dataMap[i].gueltige_stimmen}</span>
              </div>
              <div style="display: flex; justify-content: space-between; margin-bottom: 8px;">
                  <span style="font-weight: bold;">Ungültige Stimmen:</span>
                  <span>&nbsp;&nbsp;${dataMap[i].ungueltige_stimmen}</span>
              </div>
              <div style="display: flex; justify-content: space-between; margin-bottom: 8px;">
                  <span style="font-weight: bold;">Stimmbeteiligung:</span>
                  <span>&nbsp;&nbsp;${dataMap[i].stimmbeteiligung}%</span>
              </div>
              <div style="display: flex;color: #a3c308; justify-content: space-between; margin-bottom: 8px;">
                  <span style="font-weight: bold;">Ja Stimmen:</span>
                  <span>&nbsp;&nbsp;${dataMap[i].ja_stimmen}</span>
              </div>
              <div style="display: flex;color: #1e9bd3; justify-content: space-between; margin-bottom: 8px;">
                  <span style="font-weight: bold;">Nein Stimmen:</span>
                  <span>&nbsp;&nbsp;${dataMap[i].nein_stimmen}</span>
              </div>
              <div style="display: flex; justify-content: space-between; margin-bottom: 8px;">
                  <span style="font-weight: bold;">Datum:</span>
                  <span>&nbsp;&nbsp;${new Date(
                    dataMap[i].datum_abstimmung
                  ).toLocaleDateString("de-DE")}</span>
              </div>
          </div>
      </div>`;
    });

    x.addEventListener("mouseout", (event) => {
      const elTooltip = document.getElementById("tooltip-map");
      elTooltip.classList.add("do-not-display");
    });
  }
});

// Zu Beginn alle Diagram 1 Daten vom Backend laden
getAllDiagram1().then((dataDiagram1) => {
  console.log("Balken-Diagramm");
  console.log(dataDiagram1); //
  createBarChart("myChart2", dataDiagram1);
});

// Zu Beginn alle Diagram 2 Daten vom Backend laden
getAllDiagram2().then((dataDiagram2) => {
  console.log("Pie-Chart");
  console.log(dataDiagram2); //
  createPieChart("myChart3", dataDiagram2);
});

// Zu Beginn alle Diagram 3 Daten vom Backend laden
getAllDiagram3().then((dataDiagram3) => {
  console.log("Linien-Diagramm");
  console.log(dataDiagram3); //
  createLineChart("myChart1", dataDiagram3);
});
